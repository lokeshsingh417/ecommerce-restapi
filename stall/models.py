from django.db import models

# Create your models here.
import uuid
from django.contrib.auth.models import User
from datetime import datetime
class Client(models.Model):
	client_id = models.UUIDField(primary_key=True,default=uuid.uuid4,editable=False)
	full_name= models.CharField(max_length=200)
	contact_no= models.CharField(max_length=10)
	email_address=models.EmailField(max_length=200)
	super_admin=models.ForeignKey(User,on_delete=models.CASCADE)
	avator=models.ImageField(upload_to="clients",blank=True,null=True)
	address=models.TextField()
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.full_name

class Customer(models.Model):
	customer_id = models.UUIDField(primary_key=True,default=uuid.uuid4,editable=False)
	full_name= models.CharField(max_length=200)
	contact_no= models.CharField(max_length=10)
	email_address=models.EmailField(max_length=200)
	client=models.ForeignKey(Client,on_delete=models.CASCADE)
	avator=models.ImageField(upload_to="customers",blank=True,null=True)
	address=models.TextField()
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.full_name

class Product(models.Model):
	product_id= models.UUIDField(primary_key=True,default=uuid.uuid4,editable=False)
	name=models.CharField(max_length=100)
	base_price=models.FloatField()
	regular_price=models.FloatField()
	sale_price=models.FloatField()
	stock=models.IntegerField(default=0)
	description=models.TextField()
	client=models.ForeignKey(Client,on_delete=models.CASCADE)
	image=models.ImageField(upload_to="products")
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class Category(models.Model):
	category_id=models.UUIDField(primary_key=True,default=uuid.uuid4,editable=False)
	name = models.CharField(max_length=100)
	slug=models.SlugField(max_length=100)
	description=models.TextField()
	products=models.ManyToManyField(Product)
	image = models.ImageField(upload_to="category")
	client=models.ForeignKey(Client,on_delete=models.CASCADE)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class Coupon(models.Model):
	coupon_id= models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	name= models.CharField(max_length=100)
	max_use=models.IntegerField(default=1)
	face_value=models.FloatField()
	client=models.ForeignKey(Client,on_delete=models.CASCADE,default="")
	metric=models.CharField(max_length=100)
	expire_date=models.DateTimeField(default=datetime.now())
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)


class Cart(models.Model):
	cart_id = models.UUIDField(default=uuid.uuid4,primary_key=True,editable=False)
	products=models.ManyToManyField(Product)
	customer=models.ForeignKey(Customer,on_delete=models.CASCADE)
	client= models.ForeignKey(Client,on_delete=models.CASCADE)
	coupon= models.ForeignKey(Coupon,on_delete=models.SET_NULL,blank=True,null=True)
	created_at=models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)





