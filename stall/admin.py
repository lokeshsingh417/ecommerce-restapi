from django.contrib import admin
from .models import Client,Customer,Product,Category,Coupon,Cart

# Register your models here.
admin.site.register([Client,Customer,Product,Category,Coupon,Cart])