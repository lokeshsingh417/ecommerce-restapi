from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import (ClientViewSet,
					CustomerViewSet,
					ProductViewSet,
					CartViewSet,
					CouponViewSet,
					CategoryViewSet,
					checkLogin)
urlpatterns = [
path('checkLogin/<str:contact_no>/',checkLogin,name="check-login"),


]
router = DefaultRouter()
router.register('client', ClientViewSet, basename='client')
router.register('customer', CustomerViewSet, basename='customer')
router.register('cart', CartViewSet, basename='cart')
router.register('coupon', CouponViewSet, basename='coupon')
router.register('product', ProductViewSet, basename='product')
router.register('category', CategoryViewSet, basename='category')

urlpatterns+= router.urls